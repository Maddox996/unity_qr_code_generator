﻿using UnityEngine;
using ZXing;
using ZXing.QrCode;
using ZXing.Common;
using UnityEngine.UI;

namespace Quar
{
    public class QuarCodeImage : RawImage {

        QRCodeWriter QRCodeGenerator = new QRCodeWriter();

        [System.NonSerialized]
        QrCodeEncodingOptions qrCodeEncodingOptions = null;

        [SerializeField]
        string _text;
        public string text
        {
            get
            {
                return _text;
            }

            set
            {
                if (text == value)
                    return;
                _text = value;
                GenereteQRCodeRawImage();      
            }
        }

        Texture2D QRTexture;
        RawImage image;
        int lastDimension = 0;


        public void GenereteQRCodeRawImage()
        {
            if (qrCodeEncodingOptions == null)
            {
                qrCodeEncodingOptions = new QrCodeEncodingOptions
                {
                    Margin = 0,
                    PureBarcode = true
                };
            }
          
            if (text == string.Empty)
            {
                texture = Texture2D.blackTexture;
                return;
            }

            texture = GenerateQRCodeTexture();
        }

        Texture2D GenerateQRCodeTexture()
        {       
            BitMatrix bits = QRCodeGenerator.encode(text, BarcodeFormat.QR_CODE, 0, 0, qrCodeEncodingOptions.Hints);

            if (bits.Dimension != lastDimension)
            {
                if (QRTexture != null)
                {
                    if (Application.isPlaying)
                        Destroy(QRTexture);
                    else if (Application.isEditor)
                        DestroyImmediate(QRTexture);
                }

                QRTexture = new Texture2D(bits.Dimension, bits.Dimension, TextureFormat.RGBA32, false);
                QRTexture.filterMode = FilterMode.Point;
                lastDimension = bits.Dimension;
            }

            for (int i = 0; i < bits.Dimension; i++)
            {
                for (int j = 0; j < bits.Dimension; j++)
                {
                    if (bits[i, j])
                        QRTexture.SetPixel(i, j, Color.white);
                    else
                        QRTexture.SetPixel(i, j, Color.clear);
                }
            }

            QRTexture.Apply();
            return QRTexture;
        }
    }
}