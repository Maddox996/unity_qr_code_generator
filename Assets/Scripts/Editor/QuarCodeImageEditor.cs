﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Quar
{
    [CustomEditor(typeof(QuarCodeImage))]
    public class QRGeneratorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            var msg = serializedObject.FindProperty("_text").stringValue;

            QuarCodeImage generator = (QuarCodeImage)target;
            var newMsg = EditorGUILayout.TextField("Message ", msg);
            generator.color = EditorGUILayout.ColorField("Color ", generator.color);
            generator.raycastTarget = EditorGUILayout.Toggle("Raycast target ", generator.raycastTarget);

            if (msg != newMsg)
            {
                serializedObject.FindProperty("_text").stringValue = newMsg;
                serializedObject.ApplyModifiedProperties();
                generator.GenereteQRCodeRawImage();
            }
        }
    }
}